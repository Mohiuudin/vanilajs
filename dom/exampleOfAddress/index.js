"use strict";

let addressData = (function() {
const addresses = [
  {
    name: "Md Mohiuddin",
    home: "Hali Shahar Munir Nagar",
    village: "Munshipara",
    'post office': "Ananda Bazar",
    thana: "Bandar",
    district: "Chittagong"
  },
  {
    name: "Belayet Hossain",
    home: "Kazi bari",
    village: "dokkhin dewora",
    'post office': "polash",
    thana: "polash",
    district: "Narsingdhi"
  },
  {
    name: "Monir Hossain",
    home: "Master Bari",
    village: "kunjerhat",
    'post office': "kunjerhat",
    thana: "barhanuddin",
    district: "Bhola"
  }
];
return addresses;
})();
// console.log(addressData);

const Mohiuddin = {};
Mohiuddin.htmlTable = {
  buildTh: function(column) {
    let theadText = document.createTextNode(column.toUpperCase());
    let th = document.createElement("th");
    th.appendChild(theadText);
    return th;
  },

  buildTableHead: function(titles) {
    let tr = document.createElement("tr");
    
    for (let title of titles) {
      let th = this.buildTh(title);
      tr.appendChild(th);
    }
    return tr;
  },

  buildTbody: function(column) {
    let tdCell = document.createTextNode(column);
    let td = document.createElement("td");
    td.appendChild(tdCell);
    return td;
  },

  buildTableBody: function(collection) {
 
    let tbody = document.createElement('TBODY');
    for (let data of collection) {
      let tr=document.createElement("tr");
      let td = "";
      for (let proparty in data) {

        td = this.buildTbody(data[proparty]);
        tr.appendChild(td);
     
      }
     
      tbody.appendChild(tr);
      
    }
   
    return tbody;
  },

  buildTable: function(collection) {

    let table = document.createElement("table");
    let titles = Object.keys(collection[0]);
    let tHead = this.buildTableHead(titles);
    let tBody = this.buildTableBody(collection);
    table.appendChild(tHead);
    table.appendChild(tBody);
    table.setAttribute('border', '1'); // Nodes discussion
    
    return table;
  },

  display: function(addressData, domlocation) {
    
    let container = document.querySelector(domlocation);
    let table = this.buildTable(addressData);
    container.appendChild(table);

    return true;
  }

};

Mohiuddin.htmlTable.display(addressData,'#root');

