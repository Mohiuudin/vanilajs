function loadData(){
    const url = "https://raw.githubusercontent.com/algolia/datasets/master/movies/actors.json";
    fetch(url)
      .then((resp) => resp.text()) // Transform the data into json
      .then(function(data) {
         let actors = eval(data)
        
    Mohiuddin.htmlTable.display(actors,'#root');
        })
    
      }
    
      window.addEventListener('load', function() {
        
        document.getElementById('btnLoadData').addEventListener('click', loadData)
      })
      