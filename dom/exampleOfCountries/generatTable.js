"use strict";

const Mohiuddin = {};
Mohiuddin.htmlTable = {
  buildTh: function(column) {
    let theadText = document.createTextNode(column.toUpperCase());
    let th = document.createElement("th");
    th.appendChild(theadText);
    return th;
  },

  buildTableHead: function(titles) {
    let tr = document.createElement("tr");
    
    for (let title of titles) {
      let th = Mohiuddin.htmlTable.buildTh(title);
      tr.appendChild(th);
    }
    return tr;
  },

  buildTbody: function(column) {
    let tdCell = document.createTextNode(column);
    let td = document.createElement("td");
    td.appendChild(tdCell);
    return td;
  },

  buildTableBody: function(collection) {
 
    let tbody = document.createElement('TBODY');
    for (let data of collection) {
      let tr=document.createElement("tr");
      let td = "";
      for (let proparty in data) {

        td = Mohiuddin.htmlTable.buildTbody(data[proparty]);
        tr.appendChild(td);
     
      }
     
      tbody.appendChild(tr);
      
    }
   
    return tbody;
  },

  buildTable: function(collection) {

    let table = document.createElement("table");
    let titles = Object.keys(collection[0]);
    let tHead = Mohiuddin.htmlTable.buildTableHead(titles);
    let tBody = Mohiuddin.htmlTable.buildTableBody(collection);
    table.appendChild(tHead);
    table.appendChild(tBody);
    table.setAttribute('border', '1'); // Nodes discussion
    
    return table;
  },

  display: function(data, domlocation) {
    
    let container = document.querySelector(domlocation);
    let table = Mohiuddin.htmlTable.buildTable(data);
    container.appendChild(table);

    return true;
  }

};