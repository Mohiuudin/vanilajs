------------------------------post--------------------------

Objective : displaying multiple posts from a posts json (API).

0 collecting posts data

4 preparing all posts
    3 preparing single post
        1 preparing post title
        2 preparing post body
            
    ...repeat


------------------------------
0 collecting posts data:
	step 0A: prepare ajax call to fecth data 
	step 0B: collect the data


1 preparing post title:
	Step 1A: creating  container for post title. e.g: using h1 node
	Step 1B: creating TXT node.
	Step 1C: inserting txt node into container node.

2 preparing post body:
	Step 2A: creating container for post body/detail/description. e.g. p node
	Step 2B: creating TXT node for body/description content.
	Step 2C: Inserting txt into post detail container.

3 preparing single post: (qus)
	Step 3A: creating post container. e.g using div
	Step 3B: insert post title (1) and post body (2) into post container 

4 preparing all posts:  (qus)
	Step 4A: repeat 1,2,3

5 display the post:
	Step 5A: target the location
	Step 5B: insert all posts into targeted HTML tree



-------------------- comments -----------------------

Objective : displaying multiple posts from a comments json (API).

0 collecting comments data


--------------------------------

0 collecting comments data:
	Step 0A: prepare ajax call to fecth data 
	Step 0B: collect the data


1 preparing comment name:
	Step 1A: creating  container for comment name. e.g -using h1 
	Step 1B: creating TXT node.
	Step 1C: inserting txt node into container node. e.g -using em

2 preparing comment email:
	Step 2A: creating  container for comment email. 
	Step 2B: creating TXT node.
	Step 2C: inserting txt node into container node.

3 preparing comment body:
	Step 3A: creating container for comment body/detail/description. e.g -span node
	Step 3B: creating TXT node for body/description content.
	Step 3C: Inserting txt into comment detail container. e.g -span node

4 preparing single comment:
	Step 4A: creating comment container. e.g using div
	Step 4B: insert comment  name (1) and body (2) comment comment (3) into comment container
	Step 4C: creating another comment container . e.g -using div node 
	Step 4d: Indering all nodes container e.g inside div node


5 Preparing single comment in div: 
	Step 5A: creating main comment container. e.g div tag
	Step 5B:  insert single comment(4) into main comment container

6 preparing all comments:
	Step 5A: repeat 5

7 display the comment
	Step 6A: target the location
	Step 6B: insert all comments into targeted HTML tree




-------------------- photos -----------------------

Objective : displaying multiple posts from a photos json (API).

0 collecting photos data


--------------------------------

0 collecting photos data:
	Step 0A: prepare ajax call to fecth data 
	Step 0B: collect the data


1 preparing image src:
	Step 1A: creating  container for image. e.g using img node
	Step 1B: Inserting image link inot container 


2 preparing figcaption/image detail for imgae:
	Step 2A: creating  container for imgae details/descriptino content. 
	Step 2B: creating TXT node.
	Step 2C: inserting txt node into container node.

3 prepering anchor tag for imgae

	Step 3A: creating link/anchor tag for image. e.g using a node
	Step 3B: creating text node for anchor tag
	Step 3c: Inderting text node & appropiate link into container 

4 preparing single photo 

	Step 4A: creating image container. e.g using div
	Step 4b: Insert preparing/linking image source(1), prepaing figcation/image detail for image(2) and preparing anchor tag for image into image container


5 preparing all photos
	Step 5AL Repeat 1,2,3,4

6 Display the photos 

	Step 6A: target the location
	Step 6B: insert all images inot targeted html tree.

5 display the photos :
	Step 5A: target the location
	Step 5B: insert all posts into targeted HTML tree





-------------------- todo -----------------------

Objective : displaying multiple posts from a todo json (API).

0 collecting todo data:
	Step 0A: prepare ajax call to fecth data 
	Step 0B: collect the data


1 preparing todo item 1 

	Step 1A: creating container for todo item1. e.g using li node.
	Step 1B: creating another container for todo item1. e.g using input node.
	Step 1C: creating another more  container for todo item1. e.g using span node.
	Step 1B: creating text node for todo item1. 
	Step 1C: Inserting text node into container node. e.g -span


2 preparing all todo item 

	Step 1A : Repeat 1 

3 preparing single todo
	Step 1A: creating todo container. e.g -using ul
	Step 2B: Insert post preparing all todo items into todo container

4 preparing all todos

5 display the todo 
	Step 1A: target the location 
	Step 2B: Insert all Todos into targets html tree 



-------------------- users -----------------------

Objective : displaying multiple posts from a users json (API).


0 collecting users data:
	Step 0A: prepare ajax call to fecth data 
	Step 0B: collect the data

1 preparing name of user 

	Step 1A: creating container for use email e.g using li node.
	Step 1B: creating text node
	Step 1C: Inserting text node into container node

2 preparing email for user
	Step 2A: creating container for use email e.g using li node.
	Step 2B: creating text node
	Step 2C: Inserting text node into container node.


3 preparing user node of user 

	Step 3A: creating container for use name. e.g using li node.
	Step 3B: creating text node
	Step 3C: Inserting text node into container node

4 creating a part for user info(include user's name, email , user name )

	Step 4A: creating container for info. e.g using li node.
	Step 4B: Inserting name of user . email of user & user name  of into container 

5 preparing address of user 

	Step 5A: creating container for user address e.g using addr node.
	Step 5B: creating text node
	Step 5C: Inserting text node into container node

6 preparing map of use

	Step 6A: creating container for use map
	Step 6B: creating set attribute os the container 

 9 Preparing Company info of user
	Step 9A : Creating container for user company. e.g - using li node
	Step 98 : creating text node.
	Step 9C : Creating another container for more info of user company. e.g. using ul node
	Step 9D : Creating container for company name. e.g. using li node.
	Step 9E : Creating text node.
	Step 9F : Inserting text node into container. e.g - inside li node.
	Step 9G : Creating container for company Catch Phrase. e. 8 - using li node.
	Step 9H : Creating text node.
	Step 9I : Inserting text node into container. e.g - inside li node
	Step 9J : Creating container for company bs.e.g - using li node.
	Step 9K:  Creating text node.
	Step 9L:  Inserting text node into container. e.g - using li node
	Step 9M:  Inserting all (9D ato 9L) inot container. e.g - using li node
	Step 9N:  Inserting all node into container e.g - using li node

10 creating a part for user info(include user's phone, website, company)

	Step 10A: creating  container for user info e.g - using li node
	Step 10B: inserting phone info, website info, company info into container.

11 Preparing single user 
	Step 11A: creating user container e.g - using div
	Step 11B: inser user's all ingo (name of user, email of user , user name, address, map, phone info, website info, company info) into user container.

12 preparing all users 
	Step 12A: repat 1,2,3,4,5,6,7,8,9,10,11

13 display the user

	Step 13A: target the location 
	Step 13B: insert all users into targetd html tree














