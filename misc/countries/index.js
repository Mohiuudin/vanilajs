"use strict";

const Mohiuddin = {};
Mohiuddin.htmlTable = {
  buildTh: function(column) {
    let theadText = document.createTextNode(column.toUpperCase());
    let th = document.createElement("th");
    th.appendChild(theadText);
    return th;
  },

  buildTableHead: function(titles) {
    let tr = document.createElement("tr");
    
    for (let title of titles) {
      let th = this.buildTh(title);
      tr.appendChild(th);
    }
    return tr;
  },

  buildTbody: function(column) {
    let tdCell = document.createTextNode(column);
    let td = document.createElement("td");
    td.appendChild(tdCell);
    return td;
  },

  buildTableBody: function(collection) {
 
    let tbody = document.createElement('TBODY');
    for (let data of collection) {
      let tr=document.createElement("tr");
      let td = "";
      for (let proparty in data) {

        td = this.buildTbody(data[proparty]);
        tr.appendChild(td);
     
      }
     
      tbody.appendChild(tr);
      
    }
   
    return tbody;
  },

  buildTable: function(collection) {

    let table = document.createElement("table");
    let titles = Object.keys(collection[0]);
    let tHead = this.buildTableHead(titles);
    let tBody = this.buildTableBody(collection);
    table.appendChild(tHead);
    table.appendChild(tBody);
    table.setAttribute('border', '1'); // Nodes discussion
    
    return table;
  },

  display: function(data, domlocation) {
    
    let container = document.querySelector(domlocation);
    let table = this.buildTable(data);
    container.appendChild(table);

    return true;
  }

};

function loadData(){
const url = "https://gist.githubusercontent.com/keeguon/2310008/raw/bdc2ce1c1e3f28f9cab5b4393c7549f38361be4e/countries.json";
fetch(url)
  .then((resp) => resp.text()) 
  .then(function(data) {
    let countries = eval(data)
    
Mohiuddin.htmlTable.display(countries,'#root');
    })

  }

    document.getElementById('btnLoadData').addEventListener('click', loadData)
