'use strict'


const data = (function (){

    const animals = [
        {
            id:1,
            name:"kitty",
            species:"cat",
            foods: "fresh food",
        },
        {
            id:2,
            name:"Pupster",
            species:"dog",
            foods: "peas",
        },
        {
            id:3,
            name:"Tuntun",
            species:"parrot",
            foods: "fruits ",
        }
    ]
    
    return animals;
})(); // IIFE : Always one time execute

// function animals(id, name, species, foods){
//     console.log(arguments);

//  }

let table = document.createElement('table'); // Virtual DOM

for(let animal of data){
    
    let itemIdText = document.createTextNode(animal.id);
    let itemIdTd = document.createElement('td');
    itemIdTd.appendChild(itemIdText);

    let itemNameText = document.createTextNode(animal.name);
    let itemNameTd = document.createElement('td');
    itemNameTd.appendChild(itemNameText);

    let itemSpeciesText = document.createTextNode(animal.species);
    let itemSpeciesTd = document.createElement('td');
    itemSpeciesTd.appendChild(itemSpeciesText);

    let itemFoodsText = document.createTextNode(animal.foods);
    let itemFoodTd = document.createElement('td');
    itemFoodTd.appendChild(itemFoodsText);
    
    
    let tr = document.createElement('tr');  // find the reason why
    tr.appendChild(itemIdTd);
    tr.appendChild(itemNameTd);
    tr.appendChild(itemSpeciesTd);
    tr.appendChild(itemFoodTd);
    
    
    table.appendChild(tr);
    
}

console.log(table);
let container = document.querySelector("#root")
container.appendChild(table);