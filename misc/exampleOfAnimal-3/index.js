'use strict'


const data = (function (){

    const animals = [
        {
            id:1,
            name:"kitty",
            species:"cat",
            foods: "fresh food",
            like :  "celery", 
            dislike : "carrots"
        },
        {
            id:2,
            name:"Pupster",
            species:"dog",
            foods: "peas",
            like :  "dog food",
            dislike : "cat food"

        },
        {
            id:3,
            name:"Tuntun",
            species:"parrot",
            foods: "fruits ",
            like :  "canned food", 
            dislike : "dry food"
        },
        {
            id:4,
            name:"piku",
            species:"cat",
            foods: "fresh food ",
            like :  "chicken ", 
            dislike : "dim "
        }
    ]
    
    return animals;
})(); // IIFE : Always one time execute

function createTableHead(headText){
    let headContent = document.createTextNode(headText);
    let head = document.createElement('TH');
    head.appendChild(headContent);
    
    return head;
    }

    function createTableCell(cellText){
    let textNode = document.createTextNode(cellText);
    let container = document.createElement('TD');
    container.appendChild(textNode);
    
    return container;
    }

    // preparing the table for the data
    // table
    let table = document.createElement('table'); // Virtual DOM

    //thead
    let tr = document.createElement('tr');

    for(let property in data[0]){
    tr.appendChild(createTableHead(property.toUpperCase()));
    }
    table.appendChild(tr);

    // table rows
    for(let animal of data){
    let td ='';
    let tr = document.createElement('tr');  // find the reason why
    
    for(let property in animal){
        td = createTableCell(animal[property])
        tr.appendChild(td)
        
    } 
    table.appendChild(tr);
    }

    let container = document.querySelector("#root")
    container.appendChild(table);





































// function createTableHead(headerText){

//     let headContent = document.createTextNode(headerText);
//     let head = document.createElement('TH');
//     head.appendChild(headContent);

//     return head;
// }

// let table = document.createElement('table'); // Virtual DOM

// let tr = document.createElement('tr');

// tr.appendChild(createTableHead('Id'));
// tr.appendChild(createTableHead('name'));
// tr.appendChild(createTableHead('species'));
// tr.appendChild(createTableHEad('foods'));

// table.appendChild(tr);


// function createTableCell(cellText){

//     //create text node
// function createTableCell(cellText){
//     let textNode = document.createTextNode(cellText);
//     //create cell/container 
//     let container = document.createElement(TD);

//     //insert text node within container
//     container.appendChild(textNode);

//     return container;
// }

// for(let animal of data){
    
//     let cellId = createTableCell(animal.id)
//     let cellName = createTableCell(animal.name)
//     let cellSpecies = createTableCell(animal.species)
//     let cellFoods = createTableCell(animal.foods)


    
//     let tr = document.createElement('tr');  // find the reason why
//     tr.appendChild(cellId);
//     tr.appendChild(cellName);
//     tr.appendChild(cellSpecies);
//     tr.appendChild(cellFoods);
    
    
//     table.appendChild(tr);
    
// }

// console.log(table);
// console.log('-------------');
// let container = document.querySelector("#root")
// container.appendChild(table);
