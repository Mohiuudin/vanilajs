'use strict'


const data = (function (){

    const animals = [
        {
            id:1,
            name:"kitty",
            species:"cat",
            foods: "fresh food",
        },
        {
            id:2,
            name:"Pupster",
            species:"dog",
            foods: "peas",
        },
        {
            id:3,
            name:"Tuntun",
            species:"parrot",
            foods: "fruits ",
        }
    ]
    
    return animals;
})(); // IIFE : Always one time execute

function createTableHead(headerText){
    let headContent = document.createTextNode(headerText);
    let head = document.createElement('TH');
    head.appendChild(headContent);
    
    return head;
    }


let table = document.createElement('table'); // Virtual DOM

let tr = document.createElement('tr');

tr.appendChild(createTableHead('Id'));
tr.appendChild(createTableHead('Name'));
tr.appendChild(createTableHead('Species'));
tr.appendChild(createTableHead('Foods'));

table.appendChild(tr);


function createTableCell(cellText){
    // create txt node
    let textNode = document.createTextNode(cellText);
    // create cell / container
    let container = document.createElement('TD');
    // insert txt node within container
    container.appendChild(textNode);
    
    return container;
    }

    for(let animal of data){
        let cellId = createTableCell(animal.id)
        let cellName = createTableCell(animal.Name)
        let cellSpecies = createTableCell(animal.Species)
        let cellFoods = createTableCell(animal.Foods)
        
        let tr = document.createElement('tr');  // find the reason why
        tr.appendChild(cellId);
        tr.appendChild(cellName);
        tr.appendChild(cellSpecies);
        tr.appendChild(cellFoods);
        
        
        table.appendChild(tr);
        }


        
        console.log(table);
        console.log('-------------');
        let container = document.querySelector("#root")
        container.appendChild(table);







































// function createTableHead(headerText){

//     let headContent = document.createTextNode(headerText);
//     let head = document.createElement('TH');
//     head.appendChild(headContent);

//     return head;
// }

// let table = document.createElement('table'); // Virtual DOM

// let tr = document.createElement('tr');

// tr.appendChild(createTableHead('Id'));
// tr.appendChild(createTableHead('name'));
// tr.appendChild(createTableHead('species'));
// tr.appendChild(createTableHEad('foods'));

// table.appendChild(tr);


// function createTableCell(cellText){

//     //create text node
// function createTableCell(cellText){
//     let textNode = document.createTextNode(cellText);
//     //create cell/container 
//     let container = document.createElement(TD);

//     //insert text node within container
//     container.appendChild(textNode);

//     return container;
// }

// for(let animal of data){
    
//     let cellId = createTableCell(animal.id)
//     let cellName = createTableCell(animal.name)
//     let cellSpecies = createTableCell(animal.species)
//     let cellFoods = createTableCell(animal.foods)


    
//     let tr = document.createElement('tr');  // find the reason why
//     tr.appendChild(cellId);
//     tr.appendChild(cellName);
//     tr.appendChild(cellSpecies);
//     tr.appendChild(cellFoods);
    
    
//     table.appendChild(tr);
    
// }

// console.log(table);
// console.log('-------------');
// let container = document.querySelector("#root")
// container.appendChild(table);
