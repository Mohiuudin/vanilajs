
'use strict'
const data = (function (){
const products = [
    {
        id:1,
        type:"Laptop",
        brand:"HP",
        price: 100000
    },
    {
        id:2,
        type:"Mouse",
        brand:"A4",
        price: 400
    },
    {
        id:3,
        type:"Keyboard",
        brand:"Logitech",
        price: 300
    }
]

return products;
})(); // IIFE : Always one time execute


function createTableHead(headerText){
    let headContent = document.createTextNode(headerText);
    let head = document.createElement('TH');
    head.appendChild(headContent);
    
    return head;
    }


let table = document.createElement('table'); // Virtual DOM

let tr = document.createElement('tr');

tr.appendChild(createTableHead('Id'));
tr.appendChild(createTableHead('Type'));
tr.appendChild(createTableHead('Brand'));
tr.appendChild(createTableHead('Price'));

table.appendChild(tr);


function createTableCell(cellText){
    // create txt node
    let textNode = document.createTextNode(cellText);
    // create cell / container
    let container = document.createElement('TD');
    // insert txt node within container
    container.appendChild(textNode);
    
    return container;
    }

    for(let product of data){
        let cellId = createTableCell(product.id)
        let cellType = createTableCell(product.type)
        let cellBrand = createTableCell(product.brand)
        let cellPrice = createTableCell(product.price)
        
        let tr = document.createElement('tr');  // find the reason why
        tr.appendChild(cellId);
        tr.appendChild(cellType);
        tr.appendChild(cellBrand);
        tr.appendChild(cellPrice);
        
        
        table.appendChild(tr);
        }


        
        console.log(table);
        console.log('-------------');
        let container = document.querySelector("#root")
        container.appendChild(table);


















    
// let thIdText = document.createTextNode('id');
// let thId =document.createElement('th');
// thId.appendChild(thIdText);
// tr.appendChild(thId);

// let thTypeText = document.createTextNode('type');
// let thType =document.createElement('th');
// thType.appendChild(thTypeText);
// tr.appendChild(thType);

// let thBrandText = document.createTextNode('brand');
// let thBrand =document.createElement('th');
// thBrand.appendChild(thBrandText);
// tr.appendChild(thBrand);

// let thPriceText = document.createTextNode('price');
// let thPrice =document.createElement('th');
// thPrice.appendChild(thPriceText);
// tr.appendChild(thPrice);
// table.appendChild(tr);


// for(let product of data){

// let itemIdText = document.createTextNode(product.id);
// let itemIdTd = document.createElement('td');
// itemIdTd.appendChild(itemIdText);

// let itemTypeText = document.createTextNode(product.type);
// let itemTypeTd = document.createElement('td');
// itemTypeTd.appendChild(itemTypeText);

// let itemBrandText = document.createTextNode(product.brand);
// let itemBrandTd = document.createElement('td');
// itemBrandTd.appendChild(itemBrandText);

// let itemPriceText = document.createTextNode(product.price);
// let itemPriceTd = document.createElement('td');
// itemPriceTd.appendChild(itemPriceText);


// let tr = document.createElement('tr');  // find the reason why
// tr.appendChild(itemIdTd);
// tr.appendChild(itemTypeTd);
// tr.appendChild(itemBrandTd);
// tr.appendChild(itemPriceTd);


// table.appendChild(tr);
// }

// console.log(table);
// let container = document.querySelector("#root")
// container.appendChild(table);