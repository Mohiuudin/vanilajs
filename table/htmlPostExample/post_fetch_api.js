"use strict"

/*

* Objective : Displaying Multiple posts from a post JSON (API)

0 : Collecting posts data

4 : Preparing All Posts

3 : Preparing Single Post

1 : Preparing Post Title

title container

title text

2 : Preparing Post Body

body container

body text

........repeat

.............................................

0 : Collecting posts data

Step 0A : Prepare Ajax Call to fetch data.

Step 0B : Collect the data .

*/

const Mohiuddin = {};

Mohiuddin.BlogPosts = {

//1 : Preparing Post Title

getPostTitle : function(title) {

let postTitle = null;

//Step 1A : Creating container for post title . e.g - using h1 node.

let postTitleContainer = document.createElement('h1');

//Step 1B : Creating text node.

let postTitleText = document.createTextNode(title);

//Step 1C : Inserting text node into container node.

postTitleContainer.appendChild(postTitleText);

postTitle = postTitleContainer;//(container + content)

return postTitle;

},

//2 : Preparing Post Body

getPostBody: function(body){

let postBody = null;

//Step 2A : Creating container for post body/detail/description. e.g - p node.

let postBodyContainer = document.createElement('p');

//Step 2B : Creating text node for body/description content.

let postBodyText = document.createTextNode(body);

//Step 2C : Inserting text into post detail container .

postBodyContainer.appendChild(postBodyText);

postBody = postBodyContainer;//(container + content)

return postBody;

},

//3 : Preparing Single Post

getPost : function(title,body){

let post = null;

//Step 3A : Creating post container. e.g - using div

let postContainer = document.createElement('div');

let postTitle = this.getPostTitle(title);

let postBody = this.getPostBody(body);

//Step 3B : Insert post title (1) and post body (2) into post container .

postContainer.appendChild(postTitle);

postContainer.appendChild(postBody);

post = postContainer; //(container + content)

return post;

},

display : function(Location,postsData) {

//Step 5A : Target the Location

let htmlLocation = document.querySelector(Location);

for(let postData of postsData){

let post = this.getPost(postData.title,postData.body);

htmlLocation.appendChild(post);

console.log(post);

}

console.log(htmlLocation);

},

};

function loadPosts(){

fetch('https://jsonplaceholder.typicode.com/posts')

.then(response => response.json())

.then(function(posts){

//console.log(posts)

Mohiuddin.BlogPosts.display('#root', posts);

//console.log(window);

//window.data = posts;

});

}