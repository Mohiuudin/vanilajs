"use strict"




// -------------------- comments -----------------------

// Objective : displaying multiple posts from a comments json (API).

// 0 collecting comments data


// --------------------------------

// 0 collecting comments data:
// 	Step 0A: prepare ajax call to fecth data 
// 	Step 0B: collect the data

const mohiuddin = {};
mohiuddin.BlogComments = {


    // 1 preparing comment name:
    getCommentName: function(name){

        let commentName = null;
       
    
    
    // 	Step 1B: creating TXT node.
    let commentNameText = document.createTextNode(name);

    // 	Step 1A: Createing container for container name. e.g -using em
    let commentNameContainer = document.createElement('em');
         
    
    // 	Step 1C: Inserting text node into conatainer node. e.g inside em node
    commentNameContainer.appendChild(commentNameText);

    commentName = commentNameContainer;     
    
            
        return commentName;
    
    },


    // 2 preparing comment email:
    getCommentEmail: function(email){

    let commentEmail = null;

// 	Step 2A: creating  container for comment email. 
let commentEmailContainer = document.createElement('strong');
// 	Step 2B: creating TXT node.
let commentEmailText = document.createTextNode(email);

// 	Step 2C: inserting txt node into container node.

commentEmailContainer.appendChild(commentEmailText);

commentEmail = commentEmailContainer;  //(container + content)


return commentEmail;

},

// 3 preparing comment body:

getCommentBody: function(body){

    let commentBody = null; 
     

// 	Step 3A: creating container for comment body e.g -span node
let commentBodyContainer = document.createElement('span');


// 	Step 3B: creating TXT node for body  
let commentBodyText = document.createTextNode(body);


// 	Step 3C: Inserting txt into comment detail container. e.g -span node
commentBodyContainer.appendChild(commentBodyText);


      commentBody = commentBodyContainer;

        
        return commentBody;

},

// 4 preparing single comment:
getComment: function(commentData){

    let comment = null;
    
    
    // 	Step 4A: creating comment container. e.g using p node
    let commnentContainer = document.createElement('p');

    //Step 4B: Inserting commnent name (1), comments body (2), commmnet email (3); comment container  
    let commentEmail = this.getCommentEmail(commentData.email);
    let commentName = this.getCommentName(commentData.name);
    let commentBody = this.getCommentBody(commentData.body);   
   
    commnentContainer.appendChild(commentEmail);
    commnentContainer.appendChild(commentName);
    commnentContainer.appendChild(commentBody);
    

    comment = commnentContainer;

    return comment;
    
    },

    display : function(Location, commentsData) {

        //Step 5A : Target the Location
        
        let htmlLocation = document.querySelector(Location);
        
        for(let commentData of commentsData){
        
        let comment = this.getComment(commentData);
        
        htmlLocation.appendChild(comment);
        
        console.log(comment);
        
        }
    
        
        //console.log(htmlLocation);
        
 }

};

function loadComments(){

    fetch('https://jsonplaceholder.typicode.com/comments')
    
    .then(response => response.json())
    

    .then(function(comments){
    
    //console.log(posts)
    
    mohiuddin.BlogComments.display('#root', comments);
    
    
    
    });
    
    }












        







