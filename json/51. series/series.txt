series": [{
  // first series data
  "data": [
    {"x": "P1", "value": "128.14"},
    {"x": "P2", "value": "112.61"},
    {"x": "P3", "value": "163.21"},
    {"x": "P4", "value": "229.98"},
    {"x": "P5", "value": "90.54"}
  ]
},{
  // second series data
  "data": [
    {"x": "P1", "value": "90.54"},
    {"x": "P2", "value": "104.19"},
    {"x": "P3", "value": "150.67"},
    {"x": "P4", "value": "120.43"},
    {"x": "P5", "value": "200.34"}
  ]
}]